package pl.fewbits.h3;

import com.opencsv.bean.CsvBindByName;

public class Player {

    @CsvBindByName
    private String nick;

    @CsvBindByName
    private int groupNumber;

    public String getNick() {
        return nick;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }
}
