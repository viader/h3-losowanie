package pl.fewbits.h3;

import org.apache.commons.cli.*;

public class OptionsParser {

    private String outputFilenameCsv;
    private String playersToDrawCsv;
    private String playersSetCsv;
    private int groupCount;
    private int playerInGroupCount;
    private int seed;

    public OptionsParser(String... args) {
        Options options = createOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cmd = parser.parse(options, args);
            outputFilenameCsv = cmd.getOptionValue("outputFilenameCsv");
            playersToDrawCsv = cmd.getOptionValue("playersToDrawCsv");
            playersSetCsv = cmd.getOptionValue("playersSetCsv");
            groupCount = ((Number)cmd.getParsedOptionValue("groupNumber")).intValue();
            playerInGroupCount = ((Number)cmd.getParsedOptionValue("playersInGroupCount")).intValue();
            seed = ((Number)cmd.getParsedOptionValue("seed")).intValue();
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("h3-generator", options);
            System.exit(1);
        }
    }

    private Options createOptions() {
        Options options = new Options();

        Option outputFilenameCsv = new Option("o", "outputFilenameCsv", true, "CSV output filename");
        outputFilenameCsv.setRequired(true);
        options.addOption(outputFilenameCsv);

        Option playersToDrawCsv = new Option("pd", "playersToDrawCsv", true, "CSV filename to data of players to draw");
        playersToDrawCsv.setRequired(true);
        options.addOption(playersToDrawCsv);

        Option playersSetCsv = new Option("ps", "playersSetCsv", true, "CSV filename to data of actual set players");
        playersSetCsv.setRequired(true);
        options.addOption(playersSetCsv);

        Option groupNumber = new Option("g", "groupNumber", true, "Group number");
        groupNumber.setRequired(true);
        groupNumber.setType(Number.class);
        options.addOption(groupNumber);

        Option playersInGroupCount = new Option("c", "playersInGroupCount", true, "Players in group count");
        playersInGroupCount.setRequired(true);
        playersInGroupCount.setType(Number.class);
        options.addOption(playersInGroupCount);

        Option playerInGroupCount = new Option("s", "seed", true, "Seed");
        playerInGroupCount.setRequired(true);
        playerInGroupCount.setType(Number.class);
        options.addOption(playerInGroupCount);

        return options;
    }

    public String getOutputFilenameCsv() {
        return outputFilenameCsv;
    }

    public String getPlayersToDrawCsv() {
        return playersToDrawCsv;
    }

    public String getPlayersSetCsv() {
        return playersSetCsv;
    }

    public int getGroupCount() {
        return groupCount;
    }

    public int getPlayerInGroupCount() {
        return playerInGroupCount;
    }

    public int getSeed() {
        return seed;
    }
}
