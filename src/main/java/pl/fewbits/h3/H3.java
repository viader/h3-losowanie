package pl.fewbits.h3;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.*;
import java.util.*;

public class H3 {

    public static void main(String... args) {
       new H3(args);
    }

    private Map<Integer, List<Player>> groupMap = new HashMap<>();
    private List<Integer> notFullGroups = new ArrayList<>();
    private List<Player> playersToDraw;
    private String resultFilenameCsv;
    private int groupNumber;
    private int playersInGroupCount;
    private int seed;

    public H3(String... args) {
        OptionsParser optionsParser = new OptionsParser(args);
        playersToDraw = getPlayers(optionsParser.getPlayersToDrawCsv());
        resultFilenameCsv = optionsParser.getOutputFilenameCsv();
        groupNumber = optionsParser.getGroupCount();
        playersInGroupCount = optionsParser.getPlayerInGroupCount();
        seed = optionsParser.getSeed();

        initGroups(getPlayers(optionsParser.getPlayersSetCsv()));
        startDraw();
        saveResult();
    }

    private void startDraw() {
        Random generator = new Random(seed);
        while(!notFullGroups.isEmpty() && playersToDraw.size() > 0) {
            Player player = playersToDraw.get(0);
            player.setGroupNumber(notFullGroups.get((Math.abs(generator.nextInt())%notFullGroups.size())));
            addPlayerToGroup(player);
            playersToDraw.remove(0);
        }
    }

    private void saveResult() {
        List<Player> saveList = new ArrayList<>();
        for(int key: groupMap.keySet()) {
            saveList.addAll(groupMap.get(key));
        }

        try {
            Writer writer = new FileWriter(resultFilenameCsv);
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            beanToCsv.write(saveList);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        }
    }

    private List<Player> getPlayers(String filenameCsv) {
        List<Player> players = new ArrayList<>();
        try {
            players = new CsvToBeanBuilder(new FileReader(filenameCsv))
                    .withType(Player.class).build().parse();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return players;
    }

    private void initGroups(List<Player> initPlayers) {
        for(int i = 0; i<groupNumber; i++) {
            groupMap.put(i, new ArrayList<>());
            notFullGroups.add(i+1);
        }

        for(Player player: initPlayers) {
            addPlayerToGroup(player);
        }
    }

    private void addPlayerToGroup(Player player) {
        if(player.getGroupNumber() >= 1 && player.getGroupNumber() <= groupNumber) {
            List<Player> group = groupMap.get(player.getGroupNumber()-1);
            group.add(player);
            checkAndMarkIfFullGroup(group, player.getGroupNumber());
        }
    }

    private void checkAndMarkIfFullGroup(List<Player> group, int groupId) {
        if(isGroupFull(group)) {
            for(int i = 0; i<notFullGroups.size(); i++) {
                if(notFullGroups.get(i) == groupId) {
                    notFullGroups.remove(i);
                    return;
                }
            }
        }
    }

    private boolean isGroupFull(List<Player> group) {
        return group.size() >= playersInGroupCount;
    }
}